package com.example.proyectoreceta2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {


    private TextView TTlistar;
    private TextView TTlistar2;
    //private TextView TTlistar3;
    //private TextView TTlistar4;
    //private TextView TTlistar5;
    //private TextView TTlistar6;
    //private TextView TTlistar7;








    private Button BBboton;
    private TextView TTbuscador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.TTlistar = (TextView) findViewById(R.id.TTlistar);
        this.TTlistar2 = (TextView) findViewById(R.id.TTlistar2);
      //  this.TTlistar3 = (TextView) findViewById(R.id.TTlistar3);
       // this.TTlistar4 = (TextView) findViewById(R.id.TTlistar4);
       // this.TTlistar5 = (TextView) findViewById(R.id.TTlistar5);
       // this.TTlistar6 = (TextView) findViewById(R.id.TTlistar6);
        //this.TTlistar7 = (TextView) findViewById(R.id.TTlistar7);






        this.BBboton = (Button) findViewById(R.id.BBboton);
        this.TTbuscador = (TextView) findViewById(R.id.TTbuscador);



    }


    public void buscar(View view){

        String nombre = TTbuscador.getText().toString();


        String url = "https://api.edamam.com/search?q="+nombre+"&app_id=a9217e9d&app_key=f0082146a81454470315f08c5a45500c";
        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String q = respuestaJSON.getString("q");



                            JSONArray ingredientsJSON = respuestaJSON.getJSONArray("hits");
                            JSONObject recetajson = ingredientsJSON.getJSONObject(0);
                            JSONObject recetjson = recetajson.getJSONObject("recipe");
                            JSONArray health = recetjson.getJSONArray("ingredientLines");
                            String ingre = "";

                            for (int i = 0; i < health.length(); i++) {

                                ingre = ingre + "\n" +  health.getString(i);
                            }


                            JSONObject NUTRI = recetjson.getJSONObject("totalNutrients");


                            JSONObject NUTRI2 = NUTRI.getJSONObject("ENERC_KCAL");
                            String JSON = NUTRI2.getString("label");
                            String JJSON = NUTRI2.getString("quantity");

                            JSONObject NUTRI3 = NUTRI.getJSONObject("FAT");
                            String JSON1 = NUTRI3.getString("label");
                            String JJSON1 = NUTRI3.getString("quantity");

                            JSONObject NUTRI4 = NUTRI.getJSONObject("FASAT");
                            String JSON2 = NUTRI4.getString("label");
                            String JJSON2 = NUTRI4.getString("quantity");

                            JSONObject NUTRI5 = NUTRI.getJSONObject("FATRN");
                            String JSON3 = NUTRI5.getString("label");
                            String JJSON3 = NUTRI5.getString("quantity");

                            JSONObject NUTRI6 = NUTRI.getJSONObject("FAMS");
                            String JSON4 = NUTRI6.getString("label");
                            String JJSON4 = NUTRI6.getString("quantity");

                            JSONObject NUTRI7 = NUTRI.getJSONObject("FAPU");
                            String JSON5 = NUTRI7.getString("label");
                            String JJSON5 = NUTRI7.getString("quantity");

                            //JSONObject NUTRI8 = NUTRI.getJSONObject("CHOCDF");
                            //String JSON6 = NUTRI8.getString("label");
                            //String JJSON6 = NUTRI8.getString("quantity");

                            //JSONObject NUTRI9 = NUTRI.getJSONObject("FIBTG");
                            //String JSON7 = NUTRI9.getString("label");
                            //String JJSON7 = NUTRI9.getString("quantity");

                            //JSONObject NUTRI10 = NUTRI.getJSONObject("SUGAR");
                            //String JSON8 = NUTRI10.getString("");
                            //String JJSON8 = NUTRI10.getString("quantity");

                           // JSONObject NUTRI11 = NUTRI.getJSONObject("PROCNT");
                           // String JSON9 = NUTRI11.getString("label");
                            //String JJSON9 = NUTRI11.getString("quantity");

                            //JSONObject NUTRI12 = NUTRI.getJSONObject("CHOLE");
                            //String JSON10 = NUTRI12.getString("label");
                            //String JJSON10 = NUTRI12.getString("quantity");

                            //JSONObject NUTRI13 = NUTRI.getJSONObject("NA");
                            //String JSON11 = NUTRI3.getString("label");
                            //String JJSON11 = NUTRI3.getString("quantity");

                            //JSONObject NUTRI14 = NUTRI.getJSONObject("CA");
                            //String JSON12 = NUTRI4.getString("label");
                            //String JJSON12 = NUTRI4.getString("quantity");

                            //JSONObject NUTRI15 = NUTRI.getJSONObject("MG");
                            //String JSON13 = NUTRI15.getString("label");
                            //String JJSON13 = NUTRI15.getString("quantity");

                            //JSONObject NUTRI16 = NUTRI.getJSONObject("K");
                            //String JSON14 = NUTRI16.getString("label");
                            //String JJSON14 = NUTRI16.getString("quantity");

                            //JSONObject NUTRI17 = NUTRI.getJSONObject("FE");
                            //String JSON15 = NUTRI17.getString("label");
                            //String JJSON15 = NUTRI17.getString("quantity");

                           // JSONObject NUTRI18 = NUTRI.getJSONObject("ZN");
                           // String JSON16 = NUTRI18.getString("label");
                            //String JJSON16 = NUTRI18.getString("quantity");

                            //JSONObject NUTRI19 = NUTRI.getJSONObject("P");
                            //String JSON17 = NUTRI19.getString("label");
                            //String JJSON17 = NUTRI19.getString("quantity");

                            //JSONObject NUTRI20 = NUTRI.getJSONObject("VITA_RAE");
                            //String JSON18 = NUTRI20.getString("label");
                            //String JJSON18 = NUTRI20.getString("quantity");

                            //JSONObject NUTRI21 = NUTRI.getJSONObject("VITC");
                            //String JSON19 = NUTRI21.getString("label");
                            //String JJSON19 = NUTRI21.getString("quantity");

                            //JSONObject NUTRI22 = NUTRI.getJSONObject("THIA");
                            //String JSON20 = NUTRI22.getString("label");
                            //String JJSON20 = NUTRI22.getString("quantity");

                            //JSONObject NUTRI23 = NUTRI.getJSONObject("RIBF");
                            //String JSON21 = NUTRI23.getString("label");
                            //String JJSON21 = NUTRI23.getString("quantity");

                            //JSONObject NUTRI24 = NUTRI.getJSONObject("NIA");
                            //String JSON22 = NUTRI24.getString("label");
                            //String JJSON22 = NUTRI24.getString("quantity");

                            //JSONObject NUTRI25 = NUTRI.getJSONObject("VITB6A");
                            //String JSON23 = NUTRI25.getString("label");
                            //String JJSON23 = NUTRI25.getString("quantity");

                            //JSONObject NUTRI26 = NUTRI.getJSONObject("FOLDFE");
                            //String JSON24 = NUTRI26.getString("label");
                            // String JJSON24 = NUTRI26.getString("quantity");

                            //JSONObject NUTRI27 = NUTRI.getJSONObject("FOLFD");
                            //String JSON25 = NUTRI27.getString("label");
                            //String JJSON25 = NUTRI27.getString("quantity");

                            //JSONObject NUTRI28 = NUTRI.getJSONObject("VITB12");
                            //String JSON26 = NUTRI28.getString("label");
                            //String JJSON26 = NUTRI28.getString("quantity");

                            //JSONObject NUTRI29 = NUTRI.getJSONObject("VITD");
                            //String JSON27 = NUTRI29.getString("label");
                            //String JJSON27 = NUTRI29.getString("quantity");

                            //JSONObject NUTRI30 = NUTRI.getJSONObject("TOCPHA");
                            //String JSON28 = NUTRI30.getString("label");
                            //String JJSON28 = NUTRI30.getString("quantity");

                            //JSONObject NUTRI31 = NUTRI.getJSONObject("VITK1");
                            //String JSON29 = NUTRI31.getString("label");
                            // String JJSON29 = NUTRI31.getString("quantity");

                            //JSONObject NUTRI32 = NUTRI.getJSONObject("WATHER");
                            //String JSON30 = NUTRI32.getString("label");
                            //String JJSON30 = NUTRI32.getString("quantity");








                            TTlistar2.setText(JSON  + ": " + JJSON  + "\n"+
                                              JSON1 + ": " + JJSON1 + "\n"+
                                              JSON2 + ": " + JJSON2 + "\n"+
                                              JSON3 + ": " + JJSON3 + "\n"+
                                              JSON4 + ": " + JJSON4 + "\n"+
                                              JSON5 + ": " + JJSON5 + "\n"
                                              //JSON6 + ": " + JJSON6 + "\n"+
                                              //JSON7 + ": " + JJSON7 + "\n"+
                                              //JSON8 + ": " + JJSON8 + "\n"
                                              //JSON9 + ": " + JJSON9 + "\n"+
                                              //JSON10 + ": " + JJSON10 + "\n"+
                                              //JSON11 + ": " + JJSON11 + "\n"+
                                              //JSON12 + ": " + JJSON12 + "\n"+
                                              //JSON13 + ": " + JJSON13 + "\n"+
                                              //JSON14 + ": " + JJSON14 + "\n"+
                                              //JSON15 + ": " + JJSON15 + "\n"+
                                              //JSON16 + ": " + JJSON16 + "\n"+
                                              // JSON17 + ": " + JJSON17 + "\n"+
                            //                JSON18 + ": " + JJSON18 + "\n"+
                            //                JSON19 + ": " + JJSON19 + "\n"+
                            //                JSON20 + ": " + JJSON20 + "\n"+
                            //                JSON21 + ": " + JJSON21 + "\n"+
                            //                JSON22 + ": " + JJSON22 + "\n"+
                            //                JSON23 + ": " + JJSON23 + "\n"+
                            //                JSON24 + ": " + JJSON24 + "\n"+
                            //                JSON25 + ": " + JJSON25 + "\n"+
                            //                JSON26 + ": " + JJSON26 + "\n"+
                            //                JSON27 + ": " + JJSON27 + "\n"+
                            //                 JSON28 + ": " + JJSON28 + "\n"+
                            //               JSON29 + ": " + JJSON29 + "\n"+
                            //                JSON30 + ": " + JJSON30 + "\n"


                            );
                            //TTlistar3.setText(""+NUTRI3);
                            //TTlistar4.setText(""+NUTRI4);
                            //TTlistar5.setText(""+NUTRI5);
                            //TTlistar6.setText(""+NUTRI6);
                            //TTlistar7.setText(""+NUTRI7);


                            TTlistar.setText(ingre);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);

    }


}
